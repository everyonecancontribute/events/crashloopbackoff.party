# Crashloopbackoff.Party

Content for [https://crashloopbackoff.party](https://crashloopbackoff.party).

## Editing

### Gitpod

Open the project in [Gitpod](https://gitpod.io/#https://gitlab.com/everyonecancontribute/events/crashloopbackoff.party), it will install all dependencies and open the website preview automatically. 

### Web IDE in GitLab

Open the Web IDE and edit [content/index.md](content/index.md).

### Locations

The images are stored in [content/.vuepress/public/images/](content/.vuepress/public/images/)

Vuepress is configured in [content/.vuepress/config.js](content/.vuepress/config.js) and generates the static site into [public/](public/), where GitLab Pages expect them in [.gitlab-ci.yml](.gitlab-ci.yml).

## Development

Based on Vuepress. Install Yarn/npm and run the following commands.

Local dev server.

```
$ yarn content:dev
```

Generate static site content.

```
$ yarn content:build
```
