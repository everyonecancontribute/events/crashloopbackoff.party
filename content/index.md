# CrashLoopBackOff Party '22

![Partyyyyyyyyy](/images/crashloopbackoff22.png)

More info coming soon ...

## Follow us on Twitter

- [@wurstsalat](https://twitter.com/wurstsalat)
- [@solidnerd](https://twitter.com/solidnerd)
- [@_ediri](https://twitter.com/_ediri)
- [@philip_welz](https://twitter.com/philip_welz)
- [@nmeisenzahl](https://twitter.com/nmeisenzahl)
- [@dnsmichi](https://twitter.com/dnsmichi)


_[Edit this website.](https://gitlab.com/everyonecancontribute/events/crashloopbackoff.party)_
